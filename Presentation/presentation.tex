\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{bookmark}
\usepackage{minted}
\usepackage[citestyle=alphabetic]{biblatex}
\usepackage{categorycommands-subset}

\addbibresource{bibliography.bib}

\tikzset{commutative diagrams/every diagram/.style={
		row sep=2cm,
		column sep=3cm,
        },
        ampersand replacement=\&}

\title{Creating Canned Recursion in Functional Programming Languages from Category Theory}
\author{Luko van der Maas\\prof. dr. Herman Geuvers}
\institute{Computing Science\\Radboud university}
\date{}
 
 
 
\begin{document}
 
\frame{\titlepage}
 
\begin{frame}[fragile]
    \frametitle{Canned recursion}
    \pause
    \begin{minted}{haskell}
        sum :: [Int] -> Int
        sum [] = 0                   foldr (+) 0
        sum (x:xs) = x + sum xs
    \end{minted}
    \pause
    \begin{align*}
        \sumf &: \listo_{\nato} \to \nato \\
        \sumf \circ \nilf &= \zerof && \lcata[\zerof, \addf]\rcata\\
        \sumf \circ \consf &= \addf \circ (\idf\times \sumf)
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Category theory}

    \only<1>{
    \begin{center}
        \begin{tikzcd}
            A
                \ar[r, "f"]
                \ar[rr, bend right, phantom]\&
            B\&
            \phantom{C}
        \end{tikzcd}
    \end{center}
    }

    \only<2>{
    \begin{center}
        \begin{tikzcd}
            A
                \ar[r, "f"]
                \ar[rr, bend right, phantom]\&
            B
                \ar[r, "g"]\&
            C
        \end{tikzcd}
    \end{center}
    }

    \only<3>{
    \begin{center}
        \begin{tikzcd}
            A
                \ar[r, "f"]
                \ar[rr, bend right, "g \circ f"']\&
            B
                \ar[r, "g"]\&
            C
        \end{tikzcd}
    \end{center}
    }

    \only<4>{
    \begin{center}
        \begin{tikzcd}
            1
                \ar[r, "\zerof"]
                \ar[rr, bend right, "\isEvenf \circ \zerof \ = \ \truef"']\&
            \nato
                \ar[r, "\isEvenf"]\&
            \ob{Bool}
        \end{tikzcd}
    \end{center}
    }

    \only<5>{
    \begin{center}
        \begin{tikzcd}
            ()
                \ar[r, "\zerof"]
                \ar[rr, bend right, "\isEvenf \circ \zerof \ = \ \truef"']\&
            \nato
                \ar[r, "\isEvenf"]\&
            \ob{Bool}
        \end{tikzcd}
    \end{center}
    }

    \only<6>{
    \begin{center}
    \begin{tikzcd}
        \&
        1 + \listo_A \&
        \listo_A
            \ar[l, "\inrf"]
        \\
        1
            \ar[ur, "\inlf"]
            \ar[r, "\nilf"]
            \ar[dr, "\inlf"] \&
        \listo_A
            \ar[u, "\tailf"]
            \ar[d, "\headf"] \&
        A \times \listo_A
            \ar[l, "\consf"]
            \ar[u, "\sndf"]
            \ar[d, "\fstf"]
        \\
        \&
        1 + A \&
        A
            \ar[l, "\inrf"]
    \end{tikzcd}
    \end{center}}

    \only<7>{
    \begin{center}
    \begin{tikzcd}
        \&
        \eithert 1 ~ \listo_A \&
        \listo_A
            \ar[l, "\text{right}"]
        \\
        1
            \ar[ur, "\text{left}"]
            \ar[r, "\nilf"]
            \ar[dr, "\text{left}"] \&
        \listo_A
            \ar[u, "\tailf"]
            \ar[d, "\headf"] \&
        (A, \listo_A)
            \ar[l, "\consf"]
            \ar[u, "\sndf"]
            \ar[d, "\fstf"]
        \\
        \&
        \eithert 1 ~ A \&
        A
            \ar[l, "\text{right}"]
    \end{tikzcd}
    \end{center}}
\end{frame}

\begin{frame}
    \frametitle{Recursion schemes}
    \tableofcontents
\end{frame}

\section{Iteration}
\begin{frame}
    \frametitle{Iteration}

    \begin{align*}
        f &: \listo_A \to B \\
        f \circ \nilf &= c \\
        f \circ \consf &= h \circ ( \idf \times f)
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Catamorphism \cite{meijer_functional_1991}}
    \begin{align*}
        \sumf &: \listo_{\nato} \to \nato \\
        \sumf \circ \nilf &= \zerof \\
        \sumf \circ \consf &= \addf \circ (\idf\times \sumf)
    \end{align*}
    \only<2>{
    \begin{center}
        \begin{tikzcd}
            1 + \nato \times \listo_\nato
                \ar[d, "{[\nilf, \consf]}"]
                \ar[r, "{\idf + (\idf \times \sumf)}"] \&
            1 + \nato \times \nato
                \ar[d, "{[\zerof, \addf]}"]
            \\
            \listo_\nato
                \ar[r, dashed, "{\sumf}"] \&
            \nato
        \end{tikzcd}
    \end{center}}

    \only<3>{
    \begin{center}
        \begin{tikzcd}
            1 + \nato \times \listo_\nato
                \ar[d, "{[\nilf, \consf]}"]
                \ar[r, "{\idf + (\idf \times \lcata[\zerof, \sumf]\rcata)}"] \&
            1 + \nato \times \nato
                \ar[d, "{[\zerof, \sumf]}"]
            \\
            \listo_\nato
                \ar[r, dashed, "{\lcata[\zerof, \sumf]\rcata}"] \&
            \nato
        \end{tikzcd}
    \end{center}}
\end{frame}

\section{Co-iteration}
\begin{frame}
    \frametitle{Co-iteration}

    \begin{align*}
        f &: A \to \streamo_B \\
        \headf \circ f &= c \\
        \tailf \circ f &= f \circ h
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Anamorphism \cite{meijer_functional_1991}}
    \begin{align*}
        \natsf &: \nato \to \streamo_{\nato} \\
        \headf \circ \natsf &= \idf \\
        \tailf \circ \natsf &= \natsf \circ \succf
    \end{align*}

    \only<2>{
        \begin{center}
            \begin{tikzcd}
                \nato
                    \ar[d, "{\langle \idf, \succf \rangle}"]
                    \ar[r, dashed, "{\natsf}"] \&
                \streamo_{\nato}
                    \ar[d, "{\langle\headf, \tailf\rangle}"]
                \\
                \nato \times \nato
                    \ar[r, "{\idf \times \natsf}"] \&
                \nato \times \streamo_{\nato}
            \end{tikzcd}
        \end{center}
    }

    \only<3>{
        \begin{center}
            \begin{tikzcd}
                \nato
                    \ar[d, "{\langle \idf, \succf \rangle}"]
                    \ar[r, dashed, "{\lana\langle \idf, \succf \rangle\rana}"] \&
                \streamo_{\nato}
                    \ar[d, "{\langle\headf, \tailf\rangle}"]
                \\
                \nato \times \nato
                    \ar[r, "{\idf \times \lana\langle \idf, \succf \rangle\rana}"] \&
                \nato \times \streamo_{\nato}
            \end{tikzcd}
        \end{center}
    }

    
\end{frame}

\section{Primitive Recursion}
\begin{frame}
    \frametitle{Primitive Recursion}
    \begin{align*}
        f &: \nato \to A \\
        f \circ \zerof &= c \\
        f \circ \succf &= h \circ \langle f, \idf\rangle
    \end{align*}
    \pause
    \begin{itemize}
        \item paramorphism \cite{meertens_paramorphisms_1992}
    \end{itemize}
\end{frame}

\section{Course-of-Value recursion}
\begin{frame}
    \frametitle{Course-of-Value recursion}
    \begin{align*}
        f \circ \zerof &= c \\
        f \circ \succf &= h \circ \langle f \circ \zerof \circ !, \ldots , f \circ \prevf \rangle
    \end{align*}
    \pause
    \begin{itemize}
        \item Hylomorphism \cite{meijer_functional_1991,capretta_recursive_2006}
        \item Histomorphism \cite{uustalu_primitive_1999,vene_categorical_2000}
    \end{itemize}
\end{frame}

\begin{frame}[t,allowframebreaks]
    \frametitle{References}
    \printbibliography
\end{frame}

\begin{frame}
    \frametitle{Primitive Recursion}
    \begin{align*}
        f &: \nato \to A \\
        f \circ \zerof &= c \\
        f \circ \succf &= h \circ \langle f, \idf\rangle
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Paramorphism \cite{meertens_paramorphisms_1992}}

    \begin{align*}
        \factf &: \nato \to \nato \\
        \factf \circ \zerof &= \succf \circ \zerof \\
        \factf \circ \succf &= \mulf \circ \langle\factf, \idf\rangle
    \end{align*}
    \pause
    \begin{center}
        \begin{tikzcd}
            1 + \nato
                \ar[r, "{\idf + \langle \factf, \idf\rangle}"] 
                \ar[d, "{[\zerof, \succf]}"] \&
            1 + (\nato \times \nato)
                \ar[d, "{[\onef, \mulf \circ (\idf \times \succf)]}"]
            \\
            \nato
                \ar[r, dashed, "\factf"] \& 
            \nato
        \end{tikzcd}
    \end{center}
\end{frame}
 
\begin{frame}
    \frametitle{Hylomorphism}
    \begin{align*}
        \fibf &: \nato \to \nato \\
        \fibf \circ \zerof &= \onef \\
        \fibf \circ \succf \circ \zerof &= \onef \\
        \fibf \circ \succf \circ \succf &= \addf \circ  \langle \fibf \circ \succf, \fibf \rangle
    \end{align*}
    \pause
    \begin{center}
        \begin{tikzcd}
            \ff(\nato)
                \ar[r, "{\ff\lhylo \fibpref, \fibpostf \rhylo}"] \&
            \ff(\nato)
                \ar[d, "\fibpostf"]
            \\
            \nato
                \ar[u, "\fibpref"] 
                \ar[r, dashed, "{\lhylo \fibpref, \fibpostf \rhylo}"] \&
            \nato
        \end{tikzcd}
    \end{center}

    $$\mbox{\footnotesize $
        \fibpref = (\idf + \langle\idf, \prevf\rangle) \circ \prevf \ \ \ 
        \fibpostf = [\zerof, [\succf \circ \zerof \circ \sndf, \addf] \circ \distrf]$}
    $$
\end{frame}

\begin{frame}
    \frametitle{Histomorphism}
    \begin{align*}
        \fibf &: \nato \to \nato \\
        \fibf \circ \zerof &= \onef \\
        \fibf \circ \succf \circ \zerof &= \onef \\
        \fibf \circ \succf \circ \succf &= \addf \circ  \langle \fibf \circ \succf, \fibf \rangle
    \end{align*}
    \pause
    \begin{center}
    	\begin{tikzcd}
    		1 + \nu \nf^\times_{\nato}
    			\ar[d, "{\varphi}"']\& 
    		1 + \nato
    			\ar[l, "{\nf~\lana \left\langle \lhist \varphi \rhist, \innf^{-1} \right\rangle \rana}"']
    			\ar[d, bend left, "{[\zerof, \succf]}"]
    		\\
    		\nato  \&
            \nato 
                \ar[u, bend left, "\prevf"]
    			\ar[l, "{\lhist \varphi \rhist}"]
    	\end{tikzcd}
    \end{center}

    $$\varphi = [\onef, [\onef \circ \sndf, \addf \circ (\idf \times \valuef)] \circ \distrf \circ \outf]$$
\end{frame}

\end{document}

